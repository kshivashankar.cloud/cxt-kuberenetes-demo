# Elasticsearch & Kibana hosting on GKE
### Terraform Scripts to deploy EK Infracture on GKE
* **
#### Folder Structure
* *Elasticsearch Deployment* : Contains the terraform scripts to create the GKE Infracture and deploy the Elasticsearch & Kibana Instances
* *Ingress Deployment* : Contains the terraform scripts to deploy the ingress controller to expose the specified elasticsearch & kibana using https LoadBalancer

## Installation
* [Install Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
* In order for Terraform to run operations on your behalf, you must install and configure the gcloud SDK [Installing Cloud SDK](https://cloud.google.com/sdk/docs/quickstart)

## Prerequisite
* Make sure you have the below role to provision the above cluster
    1. Custom-Developer
    2. Kubernetes Engine Admin
    3. Storage Admin

* Create a service account and grant the "**Storage Object Admin**" role to it. Then download the key in json format and rename it to "**gcs.client.default.credentials_file**" paste the above file in
    1. Elasticsearch Deploymet/secrets/gcs.client.default.credentials_file

#### Command to deploy using Terraform Scripts
* **
**Setup Cloud SDK**
* After you've installed the gcloud SDK, initialize it by running the following command.
    ```sh
    gcloud init
    ```
* This will authorize the SDK to access GCP using your user account credentials and add the SDK to your PATH. This steps requires you to login and select the project you want to work in. Finally, add your account to the Application Default Credentials (ADC). This will allow Terraform to access these credentials to provision resources on GCloud.
    ```sh
    gcloud auth application-default login
    ```
**Flow to deploy whole Infrastructure with Elasticsearch & Kibana**
* *NOTE: Inside each Deployment folder "terraform.tfvars" is present, please update the variables accordlingly in file before deploying*
    1. Create the GKE cluster and deploy the Elasticsearch & kibana from "Elasticsearch Deployment" 
    * *Update the **image** variable in below yaml files (just repace the project-id)*
        1. Elasticsearch Deployment/elasticsearch_kibana_deployment/elastic.yaml (line no. 7)
        2. Elasticsearch Deployment/elasticsearch_kibana_deployment/elastic-nodeport.yaml (line no. 7)
        3. Elasticsearch Deployment/elasticsearch_kibana_deployment/kibana.yaml (line no. 7)
        4. Elasticsearch Deployment/operator/operator.yaml (line no. 366)
    2. Deploy the Ingress Controller from "Ingress_Deployment"
    * *Update the **domains** in certificate.yaml and update the **host** in ingress.yaml*

**Deploying Commands**
* Push the Elasticsearch Conatiner image to conatiner registery. Run the below commandin root folder only where cloudbuild.yaml file is present
    ```sh
    gcloud builds submit --project="your-project-id" --config cloudbuild.yaml --substitutions=_PROJECT="your-project-id" .
    ```
* Step 1: Go inside the **reqired** folder
* Step 2: Check the "**terraform.tfvars**" files and change the variable according if needed
* Step 3: Once you are ready to deploy run following commands inside particualr folder and that service will be deployed
    ```sh
    $ terraform init # Prepare your working directory for other commands
    $ terraform validate # Check whether the configuration is valid
    $ terraform plan # Show changes required by the current configuration
    $ terraform apply # Create or update infrastructure
    ```
* Step 4: If in case want to destroy anything build using above commands run below command
    ```sh
    $ terraform destroy # Destroy previously-created infrastructure
    ```

##### NOTE
* **
* After the GKE and EK deployment **Bucket name** with some other info will be printed for further use
* Use this Bucket while setting up the backup for elasticsearch

#### Elasticsearch Password
* **
* Run below commands to get the elasticsearch password
    ```sh
    $ gcloud container clusters get-credentials your-cluster-name --zone your-zone  # repalce your-cluster-name with the cluster name you deployed as well as zone
    
    $ kubectl get secret elasticsearch-es-elastic-user -o go-template='{{.data.elastic | base64decode}}'
    ```

## Reference Links
* [Elasticsearch on K8s](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html)
## Terraform Providers
* **
Below are the list of provider used for implementing the terraform script

| Provider | Link |
| ------ | ------ |
| GKE | [google_conatiner_cluster](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster) |
| Kubernets | [Kubernetes Provider](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs) |
| kubectl | [gavinbunney/kubectl](https://registry.terraform.io/providers/gavinbunney/kubectl/latest) |
