variable "ip_name" {}
variable "project_id" {}
variable "zone" {}
variable "cluster_name" {}

#define the provider with project and location
provider "google" {
  project     = var.project_id
}

# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {}

# Get your cluster-info
data "google_container_cluster" "my_cluster" {
    name     = var.cluster_name
    location = var.zone
}

#Same parameters as kubernetes provider
provider "kubectl" {
  load_config_file       = false
  host                   = "https://${data.google_container_cluster.my_cluster.endpoint}"
  token                  = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.my_cluster.master_auth[0].cluster_ca_certificate)
}

# Create the SSL certificates for the domains
resource "kubectl_manifest" "certificates" {
    yaml_body = file("certificate.yaml")
}

# Create the Ingress Load Balancer 
resource "kubectl_manifest" "ingress_http" {
    yaml_body = file("ingress.yaml")
    depends_on = [
      kubectl_manifest.certificates
    ]
}