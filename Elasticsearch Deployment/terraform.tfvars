#populate the below varibales according to your usecase
project_id = ""                                 # gcp project id
zone = ""                                       # zone to launch the gke cluster
cluster_name = ""                               # name of the gke cluster
channel = "UNSPECIFIED"                         # default is set "static version" if need change accordlingly

ip_name = ""                                    # name for static ip address

create_vpc = 0                                  # 1 -> to create a vpc / 0 -> to skip vpc creation
#(NOTE: if create_vpc = 0 then put vpc_name of vpc already present in project )
#(      if create_vpc = 1 then put vpc_name of you choice and vpc with that name will be created and used) 
vpc_name = ""                                   # name of the vpc  

private_cluster = true                          # change to false if if need public cluster
private_endpoint = false                        # change to true if needed private endpoint 
ipv4_cidr = "172.16.0.0/28"                     # change if address already in use

enable_elasticsearch_node = 1
nodepool_name = "elasticsearch"
nodepool_count = 1
nodepool_zone = "us-central1-c"
nodepool_vm = "n2d-standard-4"                  # change according to you use case
disk_size = 20
disk_type = "pd-ssd"

bucket_name = ""                                # globally unique name
bucket_location = ""                            # region for bucket