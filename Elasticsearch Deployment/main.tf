# define variable to be used
variable "project_id" {}
variable "zone" {}
variable "cluster_name" {}

variable "enable_elasticsearch_node" {}
variable "nodepool_name" {}
variable "nodepool_count" {}
variable "nodepool_zone" {}
variable "nodepool_vm" {}
variable "disk_size" {}
variable "disk_type" {}

variable "private_cluster" {}
variable "private_endpoint" {}
variable "ipv4_cidr" {}
variable "channel" {}

variable "vpc_name" {}
variable "create_vpc" {}

variable "bucket_name" {}
variable "bucket_location" {}

variable "ip_name" {}

#define the provide with project and location
provider "google" {
  project     = var.project_id
}

#create a vpc network in the project
resource "google_compute_network" "vpc_network" {
  count = var.create_vpc
  name                    = var.vpc_name
  auto_create_subnetworks = true
  mtu                     = 1460
}

# GKE Public cluster 
resource "google_container_cluster" "primary" {
  name     = var.cluster_name
  location = var.zone
  remove_default_node_pool = true
  initial_node_count       = 1
  network = var.vpc_name
  ip_allocation_policy {}
  private_cluster_config {
    enable_private_nodes = var.private_cluster
    enable_private_endpoint = var.private_endpoint
    master_ipv4_cidr_block = var.ipv4_cidr
  }
  release_channel {
    channel = var.channel
  }
  depends_on = [
    google_compute_network.vpc_network
  ]
}

# Managed Node Pool for Elasticsearch deployment
resource "google_container_node_pool" "elasticsearch_node" {
  count      = var.enable_elasticsearch_node
  name       = var.nodepool_name
  location   = var.nodepool_zone
  cluster    = google_container_cluster.primary.name
  node_count = var.nodepool_count
  
  management {
    auto_repair = true
    auto_upgrade = false
  }
  node_config {

    disk_size_gb = var.disk_size
    disk_type    = var.disk_type
    
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    labels = {
      type = "elasticsearch" 
    }

    machine_type = var.nodepool_vm
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  depends_on = [
    google_container_cluster.primary
  ]
}

# ---------------------------------------------------- Bucket Deployment ------------------------------------------------------#
# Bucket to store elasticsearch data backup
resource "google_storage_bucket" "elasticsearch_data_backup" {
  name          = var.bucket_name
  location      = var.bucket_location
  force_destroy = true
  lifecycle_rule {
    condition {
      age = 14
      matches_storage_class = ["STANDARD"]
    }
    action {
      type = "SetStorageClass"
      storage_class = "NEARLINE"
    }
  }
  lifecycle_rule {
    condition {
      age = 21
      matches_storage_class = ["NEARLINE"]
    }
    action {
      type = "SetStorageClass"
      storage_class = "COLDLINE"
    }
  }
  lifecycle_rule {
    condition {
      age = 28
      matches_storage_class = ["COLDLINE"]
    }
    action {
      type = "SetStorageClass"
      storage_class = "ARCHIVE"
    }
  }
}

# ---------------------------------------------------- Elasticsearch & kibana Deployment --------------------------------------#

# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {}

# Get your cluster-info
data "google_container_cluster" "cluster" {
    name     = var.cluster_name
    location = var.zone
    depends_on = [
      google_container_node_pool.elasticsearch_node
    ]
}

provider "kubernetes" {
  host                   = "https://${data.google_container_cluster.cluster.endpoint}"
  token                  = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.cluster.master_auth[0].cluster_ca_certificate)
}

#Same parameters as kubernetes provider
provider "kubectl" {
  load_config_file       = false
  host                   = "https://${data.google_container_cluster.cluster.endpoint}"
  token                  = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.cluster.master_auth[0].cluster_ca_certificate)
}

#deploy the es crds on the cluster
data "kubectl_file_documents" "crds" {
    content = file("operator/crds.yaml")
    depends_on = [
      data.google_container_cluster.cluster
    ]
}

resource "kubectl_manifest" "crds_deploy" {
    for_each  = data.kubectl_file_documents.crds.manifests
    yaml_body = each.value
}

#deploy the es operator on the cluster
data "kubectl_file_documents" "operator" {
    content = file("operator/operator.yaml")
}

resource "kubectl_manifest" "operator_deploy" {
    for_each  = data.kubectl_file_documents.operator.manifests
    yaml_body = each.value
    depends_on = [
      kubectl_manifest.crds_deploy
    ]
}

# create the secret of service account file
resource "kubernetes_secret_v1" "gcs-credentials" {
  metadata {
    name = "gcs-credentials"
  }

  data = {
    "gcs.client.default.credentials_file" =  file("secrets/gcs.client.default.credentials_file")
  }
}

#deploy the elasticsearch pod on cluster
resource "kubectl_manifest" "elastic_data" {
    yaml_body = file("elasticsearch_kibana_deploy/elastic.yaml")
    depends_on = [
      kubectl_manifest.operator_deploy
    ]
}

resource "time_sleep" "wait_elastic_data" {
  depends_on = [kubectl_manifest.elastic_data]

  create_duration = "120s"
}

#deploy the kibana pod on cluster 
resource "kubectl_manifest" "kibana_data" {
    yaml_body = file("elasticsearch_kibana_deploy/kibana.yaml")
    depends_on = [
      time_sleep.wait_elastic_data
    ]
}

resource "time_sleep" "wait_elastic_data_nodeport" {
  depends_on = [kubectl_manifest.kibana_data]

  create_duration = "180s"
}

resource "kubectl_manifest" "elastic_data_nodeport" {
    yaml_body = file("elasticsearch_kibana_deploy/elastic-nodeport.yaml")
    depends_on = [
      time_sleep.wait_elastic_data_nodeport
    ]
}

# ------------------------------------------------------ Static IP for Ingress ------------------------------------------------------ #

resource "google_compute_global_address" "default" {
    name = var.ip_name
}