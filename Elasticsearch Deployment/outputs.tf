output "zone" {
  value       = var.zone
  description = "GCloud Zone"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.primary.endpoint
  description = "GKE Cluster Host"
}

output "data_backup" {
    value = google_storage_bucket.elasticsearch_data_backup.name
    description = "GCP bucket to store elasticsearch data backup"
}

output "external_ip" {
  value = google_compute_global_address.default.address
  description = "The ingress External IP address"
}